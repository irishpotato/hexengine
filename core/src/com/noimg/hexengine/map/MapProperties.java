package com.noimg.hexengine.map;

/**
 * MapProperties создается MapLoader'ом. В этом классе хранятся ширина/высота карты и (в будущем) другие ее особенности
 */

public class MapProperties {
	private final int width;
	private final int height;
	
	public MapProperties (int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public int getWidth () {
		return width;
	}
	
	public int getHeight () {
		return height;
	}
}
