package com.noimg.hexengine.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;
import com.noimg.hexengine.utils.GzipUtil;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * MapLoader загружает и обрабатывает .tmx файлы с игровой картой
 */

public class MapLoader {
	
	/**
	 * Метод load загружает .tmx карту. Он делает это поиском в xml-файле тега data и тега map:
	 * Под тегом data располагается зашифрованная в Base64 и сжатая gzip'ом строка с идентификаторами тайлов, а под тегом map
	 * находятся свойства карты, такие как ее размеры.
	 */
	public Map load (String path) {
		try {
			FileHandle file = Gdx.files.internal (path);
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance ();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder ();
			
			Document document = documentBuilder.parse (new InputSource (new ByteArrayInputStream (file.readString ().getBytes ("utf-8"))));
			String mapData = document.getElementsByTagName ("data").item (0).getTextContent ().trim ().replaceAll ("\n", "").trim ().replaceAll (" ", "");
			int width = Integer.parseInt (document.getElementsByTagName ("map").item (0).getAttributes ().getNamedItem ("width").getNodeValue ());
			int height = Integer.parseInt (document.getElementsByTagName ("map").item (0).getAttributes ().getNamedItem ("height").getNodeValue ());
			short[][] map = getMapFromData (mapData, width, height);
			MapProperties properties = new MapProperties (width, height);
			
			return new Map (map, properties);
		} catch (Exception e) {
			Gdx.app.log ("ERROR", e.toString ());
		}
		
		return null;
	}
	
	/**
	 * Метод, декодирующий (Base64) и разжимающий (gzip) зашифрованную строку с картой
	 */
	private short[][] getMapFromData (String data, int width, int height) {
		short[][] map = new short[width][height];
		byte[] decodedBytes = Base64Coder.decode (data);
		byte[] uncompressedBytes = GzipUtil.unzip (decodedBytes).getBytes ();
		
		ByteBuffer bb = ByteBuffer.wrap (uncompressedBytes);
		bb.order (ByteOrder.LITTLE_ENDIAN);
		
		int xindex = 0;
		int yindex = height - 1;
		
		while (bb.hasRemaining ()) {
			int v = bb.getInt ();
			map[xindex][yindex] = (short) v;
			
			xindex += 1;
			if (xindex >= width) {
				xindex = 0;
				yindex -= 1;
			}
			
			if (yindex < 0) {
				break;
			}
		}
		
		return map;
	}
}
