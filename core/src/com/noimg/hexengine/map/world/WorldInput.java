package com.noimg.hexengine.map.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.utils.Point;

/**
 * Класс для обработки входных данных в игровом мире
 */

public class WorldInput implements InputProcessor, GestureDetector.GestureListener {
	
	private final World world;
	
	public WorldInput (World world) {
		this.world = world;
	}
	
	@Override
	public boolean keyDown (int keycode) {
		
		if (keycode == Input.Keys.J) {
			HexEngine.JESUS_MODE = !HexEngine.JESUS_MODE;
		}
		
		if (keycode == Input.Keys.Z) {
			HexEngine.SHOW_HEX_COORD = !HexEngine.SHOW_HEX_COORD;
		}
		
		if (keycode == Input.Keys.X) {
			HexEngine.SHOW_AXIS = !HexEngine.SHOW_AXIS;
		}
		
		if (keycode == Input.Keys.D) {
			HexEngine.SHOW_DEBUG = !HexEngine.SHOW_DEBUG;
		}
		
		return true;
	}
	
	@Override
	public boolean keyUp (int keycode) {
		return false;
	}
	
	@Override
	public boolean keyTyped (char character) {
		return false;
	}
	
	@Override
	public boolean touchDown (int screenX, int screenY, int pointer, int button) {
		world.select (getHexUnderCursor ());
		return true;
	}
	
	private Hex getHexUnderCursor () {
		Vector3 mousePosition = new Vector3 (Gdx.input.getX (), Gdx.input.getY (), 0);
		HexEngine.game.camera.unproject (mousePosition);
		return Hex.pixelToHex (new Point ((int) mousePosition.x, (int) mousePosition.y));
	}
	
	@Override
	public boolean touchUp (int screenX, int screenY, int pointer, int button) {
		return true;
	}
	
	@Override
	public boolean touchDragged (int screenX, int screenY, int pointer) {
		return false;
	}
	
	@Override
	public boolean mouseMoved (int screenX, int screenY) {
		return false;
	}
	
	@Override
	public boolean scrolled (int amount) {
		if (amount != 0) {
			HexEngine.game.camera.zoom += HexEngine.CAM_ZOOM_SPEED * amount;
			HexEngine.game.camera.update ();
			return true;
		}
		
		return false;
	}
	
	
	@Override
	public boolean touchDown (float x, float y, int pointer, int button) {
		return false;
	}
	
	@Override
	public boolean tap (float x, float y, int count, int button) {
		if (count == 2) {
			HexEngine.game.camera.zoom = 0.5f;
			HexEngine.game.camera.update ();
			return true;
		}
		
		if (count == 3) {
			HexEngine.JESUS_MODE = !HexEngine.JESUS_MODE;
		}
		
		world.select (getHexUnderCursor ());
		
		if (world.getSelection () == null)
			return false;
		if (!getHexUnderCursor ().equals (world.getSelection ())) {
			world.select (Hex.CENTER);
			return false;
		}
		
		if (world.getPlayer().canMoveTo (world.getSelection ())) {
			world.getPlayer().startMove (world.getSelection ());
		}
		return false;
	}
	
	@Override
	public boolean longPress (float x, float y) {
		return false;
	}
	
	@Override
	public boolean fling (float velocityX, float velocityY, int button) {
		return false;
	}
	
	@Override
	public boolean pan (float x, float y, float deltaX, float deltaY) {
		return false;
	}
	
	@Override
	public boolean panStop (float x, float y, int pointer, int button) {
		return false;
	}
	
	@Override
	public boolean zoom (float initialDistance, float distance) {
		HexEngine.game.camera.zoom += (initialDistance - distance) / 10000;
		HexEngine.game.camera.update ();
		return false;
	}
	
	@Override
	public boolean pinch (Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}
	
	@Override
	public void pinchStop () {
		
	}
}
