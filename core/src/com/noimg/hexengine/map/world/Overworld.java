package com.noimg.hexengine.map.world;

import com.noimg.hexengine.map.place.Place;
import com.noimg.hexengine.map.place.dungeon.DevDungeon;
import com.noimg.hexengine.map.place.settlement.DevCapital;
import com.noimg.hexengine.screens.GameScreen;

public class Overworld extends World {
	
	public Overworld (GameScreen game) {
		super (game);
	}
	
	@Override
	protected String getMapPath () {
		return "earth.tmx";
	}
	
	@Override
	protected Place[] getPlaces () {
		return new Place[]{new DevCapital (), new DevDungeon ()};
	}
	
	@Override
	public String getName () {
		return "Overworld";
	}
	
	@Override
	public String getDescription () {
		return "Only for developers";
	}
	
	//getLootList, etc.
}
