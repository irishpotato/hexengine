package com.noimg.hexengine.map.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimg.hexengine.Assets;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.creatures.Player;
import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.map.Map;
import com.noimg.hexengine.map.MapLoader;
import com.noimg.hexengine.map.place.Place;
import com.noimg.hexengine.map.place.event.Caravan;
import com.noimg.hexengine.screens.GameScreen;
import com.noimg.hexengine.utils.Debug;
import com.noimg.hexengine.utils.Point;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * World является базовым классом для игровых миров
 */

public abstract class World {
	
	private final SpriteBatch batch;
	private final Debug debug;
	private final Map map;
	private Hex selection;
	private boolean showSelection;
	private ArrayList <Hex> renderHexes;
	private ArrayList <Hex> moveHexes;
	private final Player player;
	private final InputMultiplexer input;
	private final Random random = new Random ();
	private final GameScreen game;
	private final Stage stage;
	
	protected abstract String getMapPath ();
	
	protected abstract Place[] getPlaces ();
	
	public abstract String getName ();
	
	public abstract String getDescription();
	
	public World (GameScreen game) {
		this.game = game;
		
		batch = new SpriteBatch ();
		debug = new Debug ();
		map = new MapLoader ().load (getMapPath ());	// Карта загружается MapLoader'ом
		player = new Player (this, new Hex (16, 9));
		
		updateFog ();
		updateCamera ();
		
		// InputMultiplexer используется для поддержки жестов на android'е и поддержки управления с клавиатуры-мышки
		input = new InputMultiplexer();
		input.addProcessor (new GestureDetector (new WorldInput (this)));
		input.addProcessor (new WorldInput (this));
		
		Gdx.input.setInputProcessor (input);
		
		// Настраиваем scene2d для работы с ui интерфейсом в мире
		// Возможно, в дальнейшем это уйдет в отдельный класс
		Viewport viewport = new ExtendViewport (212, 284);
		viewport.apply ();	//maybe useless
		stage = new Stage (viewport);
		
		Skin skin = HexEngine.assets.get (Assets.UISKIN, Skin.class);
		skin.getFont("default-font").getData().markupEnabled = true;
		
		Label title = new Label (getName (), skin, "title");
		Calendar calendar = new GregorianCalendar ();
		String gameDate = (calendar.get(Calendar.YEAR) - 2016) + "E " + calendar.get(Calendar.DAY_OF_YEAR);
		Label date = new Label (gameDate, skin, "default");
		title.setColor (new Color(1, 1, 1, 0));
		date.setColor (new Color(1, 1, 1, 0));
		title.addAction (sequence(Actions.delay (1), Actions.fadeIn (0),  Actions.delay (3), Actions.fadeOut (1)));
		date.addAction (sequence(Actions.delay (2), Actions.fadeIn (0), Actions.delay (2), Actions.fadeOut (1)));
		
		Table titleTable = new Table();
		titleTable.setFillParent (true);
		titleTable.add (title).align (Align.center).row ();
		titleTable.add (date).align (Align.center);
		
		stage.addActor (titleTable);
	}
	
	public void render (float delta) {
		if (player.isMoving) movePlayer (delta);
		
		Gdx.gl.glClear (GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor (0, 0, 0, 1f);
		batch.setProjectionMatrix (game.camera.combined);
		batch.begin ();
		//fixme оптимизировать рендер
		drawHexes ();
		drawObjects ();
		batch.end ();
		
		drawUI ();
		
		if (HexEngine.SHOW_DEBUG)
			debug.debug (game);
	}
	
	public void resize (int width, int height) {
		stage.getViewport().update(Math.round(width / 2) * 2, Math.round(height / 2) * 2);
		stage.getCamera().position.set(212/2, 284/2, 0);
		stage.getCamera().update();
	}
	
	private void movePlayer (float delta) {
		updateCamera ();
		
		// Если игрок закончил движение
		if (player.move (delta)) {
			playerStep();
		}
	}
	
	private void playerStep () {
		game.time ++;
		updateFog ();
		callRandomEvent ();
		
		player.foodAmount -= map.getTile (player.hex).foodNeed ();
		
		// Проверяем, не закончилась ли еда у игрока
		if (player.foodAmount <= 0) {
			player.foodAmount = 0;
			player.die ();
		}
		
		// Проверяем, не находимся ли мы на какой либо локации
		for (Place place : getPlaces ()) {
			if (player.hex.equals (place.getLocation ())) {
				HexEngine.enterPlace (place);
				return;
			}
		}
	}
	
	private void callRandomEvent () {
		if (random.nextFloat () < map.getTile (player.hex).getEventRarity ().get (Caravan.class))
			HexEngine.startEvent (new Caravan ());
	}
	
	public Color getTimeOfDayColor () {
		float sin = (float)Math.sin (game.time / HexEngine.DAYSPEED);
		return new Color(1.2f + sin, 1.2f + sin, 1.4f + sin, 1);
	}
	
	private void drawHexes () {
		for (Hex hex : renderHexes) {
			if (map.getTile (hex) == null)
				continue;
			if (!map.getTile (hex).discovered)
				continue;
			
			Color drawColor = getTimeOfDayColor ();
			batch.setColor (drawColor);
			
			if (!moveHexes.contains (hex))
				batch.setColor (new Color (drawColor.r / 1.125f, drawColor.g / 1.125f, drawColor.b / 1.125f, 1f));
			
			if (showSelection && selection.equals (hex) && !selection.equals (player.hex))
				batch.setColor (new Color (0.9f, 0.1f, 0.2f, 0.3f));
			
			if (HexEngine.SHOW_AXIS)
				debug.drawAxis (batch, hex);
			
			Point offset = Hex.cubeToOffset (hex);
			int drawx = offset.x * 32;
			int drawy = offset.y * 34 + (offset.x % 2 == 0 ? 17 : 0);
			
			TextureRegion texture = HexEngine.assets.get (Assets.ATL_TILE, TextureAtlas.class).findRegion (map.getTile (hex).getTexture ());
			
			for (Place place : getPlaces ()) {
				if (hex.equals (place.getLocation ())) {
					texture = HexEngine.assets.get (Assets.ATL_TILE, TextureAtlas.class).findRegion (place.getTexture ());
				}
			}
			
			batch.draw (texture, drawx, drawy);
			
			if (HexEngine.SHOW_HEX_COORD)
				debug.drawCoordinates (batch, hex, drawx, drawy);
		}
	}
	
	private void drawObjects () {
		batch.draw (HexEngine.assets.get (Assets.ATL_OBJECT, TextureAtlas.class).findRegion ("player"), player.position.x, player.position.y + 17);
		
		/*
		for (Hex hex : renderHexes) {
			if (map.getTile (hex) == null)
				continue;
			// Если тайл не исследован, не рендерим его (это лучше чем черный цвет в плане производительности)
			if (!map.getTile (hex).discovered)
				continue;
			
			batch.setColor (Color.WHITE);
			
			if (player.hex.equals (hex)) {
				batch.draw (HexEngine.assets.get (Assets.ATL_OBJECT, TextureAtlas.class).findRegion ("player"), player.position.x, player.position.y + 17);
			}
		}*/
	}
	
	private void drawUI () {
		stage.act (Gdx.graphics.getDeltaTime ());
		stage.draw ();
		
		// fixme временно убран рендер названий локаций, нужно переписать для работы со scene2d
		/*
		for (Place place : getPlaces ()) {
			if (!place.showName ())
				continue;
			if (!renderHexes.contains (place.getLocation ()))
				continue;
			if (!map.getTile (place.getLocation ()).discovered)
				continue;
			
			Point offset = Hex.cubeToOffset (place.getLocation ());
			int drawx = offset.x * 32;
			int drawy = offset.y * 34 + (offset.x % 2 == 0 ? 17 : 0);
			
			font.getData ().setScale (0.8f, 0.8f);
			font.setColor (Color.BLACK);
			font.draw (batch, place.getName (), drawx - 7, drawy + 40);
			font.setColor (Color.WHITE);
			font.draw (batch, place.getName (), drawx - 7, drawy + 41);
		}*/
	}
	
	private void updateFog () {
		if (map.getTile (player.hex) != null)
			map.getTile (player.hex).discovered = true;
		
		for (Hex hex : map.getNeighbors (player.hex)) {
			if (map.getTile (hex) != null) {
				map.getTile (hex).discovered = true;
			}
		}
		
		moveHexes = map.getNeighbors (player.hex);
		moveHexes.add (player.hex);
		
		renderHexes = map.getHexesInRange (player.hex, HexEngine.RENDER_RANGE);
	}
	
	public void dispose () {
		batch.dispose ();
		debug.batch.dispose ();
	}
	
	private void updateCamera () {
		game.camera.position.set (player.position.x + 17, player.position.y + 34, 0);
		game.camera.update ();
	}
	
	public InputMultiplexer getInput () {
		return input;
	}
	
	public Map getMap () {
		return map;
	}
	
	public Player getPlayer () {
		return player;
	}
	
	void select (Hex hex) {
		showSelection = true;
		selection = hex;
	}
	
	Hex getSelection () {
		return selection;
	}
}
