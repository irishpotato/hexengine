package com.noimg.hexengine.map.tile;

import com.noimg.hexengine.map.place.event.Caravan;
import com.noimg.hexengine.map.place.event.WorldEvent;

import java.util.HashMap;

public class Water extends Tile {
	
	@Override
	public String getTexture () {
		return "water";
	}
	
	@Override
	public int foodNeed () {
		return 0;
	}
	
	@Override
	public HashMap<Class <? extends WorldEvent>, Float> getEventRarity ()  {
		HashMap<Class<? extends WorldEvent>, Float> map = new HashMap<>();
		map.put (Caravan.class, 0f);
		return map;
	}
	
}
