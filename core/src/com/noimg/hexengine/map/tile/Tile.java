package com.noimg.hexengine.map.tile;

import com.noimg.hexengine.map.place.event.WorldEvent;

import java.util.HashMap;

/**
 * Tile является тем, на что делится игровая карта. Это основной класс для всех тайлов.
 */

public abstract class Tile {
	
	public boolean discovered = false;
	
	public abstract String getTexture ();
	
	// foodNeed - колличество еды, которое должен потратить игрок на проход по этому тайлу
	// При этом если foodNeed равен 0, то тайл является непроходимым
	public abstract int foodNeed ();
	
	public abstract HashMap<Class<? extends WorldEvent>, Float> getEventRarity ();
	
	//getMobRarity ...
	
}
