package com.noimg.hexengine.map.tile;

import com.noimg.hexengine.map.place.event.Caravan;
import com.noimg.hexengine.map.place.event.WorldEvent;

import java.util.HashMap;

public class Snow extends Tile {
	
	@Override
	public String getTexture () {
		return "snow";
	}
	
	@Override
	public int foodNeed () {
		return 4;
	}
	
	@Override
	public HashMap<Class <? extends WorldEvent>, Float> getEventRarity ()  {
		HashMap<Class<? extends WorldEvent>, Float> map = new HashMap<>();
		map.put (Caravan.class, 0.5f);
		return map;
	}
	
}
