package com.noimg.hexengine.map.place;

import com.noimg.hexengine.hexagon.Hex;

/**
 * Интерфейс для игровых локаций.
 * Локацией может являтся, например, город или данж
 */

public interface Place extends com.noimg.hexengine.map.place.event.WorldEvent {
	
	Hex getLocation ();
	
	String getTexture ();
	
	boolean showName ();

}
