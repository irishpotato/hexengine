package com.noimg.hexengine.map.place.settlement;

import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.map.place.Answer;

import java.util.ArrayList;

public class DevCapital implements Settlement {
	
	@Override
	public String getName () {
		return "Dev's Capital";
	}
	
	//getDialogue в городе используется, если игрок говорит с королем(или кем то там еще)
	@Override
	public ArrayList<Answer> getDialogue () {
		return null;
	}
	
	@Override
	public boolean showName () {
		return true;
	}
	
	@Override
	public String getTexture () {
		return "capital";
	}
	
	@Override
	public Hex getLocation () {
		return new Hex (15, 8);
	}

	@Override
	public String getDescription () {
		return "There are two chairs. On one there is a pie. On the other, a dick. You have to sit on one, and eat the other. Which do you choose?";
	}
	
	@Override
	public String getEntryBackground () {
		return "devcapital";
	}
	
	@Override
	public String getTavernBackground () {
		return "background";
	}
	
	@Override
	public Place[] getPlaces () {
		return new Place [] {Place.TAVERN, Place.SHOP, Place.TAVERN, Place.SHOP, Place.SHOP};
	}
}
