package com.noimg.hexengine.map.place.settlement;

import com.noimg.hexengine.map.place.Place;

/**
 * Интерфейс, реализуемый населенными пунктами
 */

public interface Settlement extends Place {
	
	//todo может быть, использовать HashMap<String, String>, где в качестве ключа будет место в локации (таверна, дом и т.д.) а в качестве значения - текстура?
	String getEntryBackground ();
	
	String getTavernBackground ();
	
	Place[] getPlaces ();
	
	enum Place {
		TAVERN, SHOP
	}
}
