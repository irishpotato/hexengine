package com.noimg.hexengine.map.place.event;

import com.noimg.hexengine.map.place.Answer;

import java.util.ArrayList;

public interface WorldEvent {
	
	String getName ();
	
	String getDescription ();
	
	ArrayList<Answer> getDialogue ();
	
}
