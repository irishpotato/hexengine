package com.noimg.hexengine.map.place.event;

import com.noimg.hexengine.map.place.Answer;

import java.util.ArrayList;

public class Caravan implements WorldEvent {
	
	public enum Action {
		LEAVE, TALK, TRADE, ATTACK
	}
	
	@Override
	public String getName () {
		return "Trade caravan";
	}
	
	@Override
	public ArrayList <Answer> getDialogue () {
		ArrayList <Answer> answers = new ArrayList <> ();
		
		answers.add (new Answer ("Talk with caravan leader", Action.TALK));
		answers.add (new Answer ("Trade", Action.TRADE));
		answers.add (new Answer ("Attack caravan", Action.ATTACK));
		answers.add (new Answer ("Leave", Action.LEAVE));
		
		return answers;
	}
	
	@Override
	public String getDescription () {
		return "Lorem ipsum justo, sodales, nibh, leo tempus quam massa in nulla ornare bibendum non odio lorem metus sem orci auctor pellentesque sapien malesuada in. Vulputate diam morbi tellus donec maecenas amet eros elementum pharetra molestie ornare orci risus - bibendum rutrum. Pellentesque non sed eros sit proin: maecenas magna non auctor, sapien ligula.\n" + "\n" + "Eget leo rutrum ornare massa, pharetra ipsum congue enim: metus sagittis duis, pellentesque non - commodo porttitor ut ipsum porta. In arcu quisque diam bibendum adipiscing orci diam vitae at ornare leo integer porta molestie, porttitor, diam gravida ipsum, rutrum integer metus vitae. Auctor tellus: quam non tempus, diam metus: proin - at congue: quisque gravida ipsum justo sodales lorem. Proin adipiscing sagittis porta sagittis - eu, sit magna pellentesque nulla risus, integer, duis ultricies. Adipiscing mauris magna fusce diam tempus, at mattis rutrum arcu maecenas pellentesque: sit nulla, ultricies porta ipsum bibendum, rutrum magna, sodales proin.";
	}
}
