package com.noimg.hexengine.map.place.dungeon;

import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.map.place.Answer;

import java.util.ArrayList;

public class DevDungeon implements Dungeon {
	
	@Override
	public Hex getLocation () {
		return new Hex (16, 8);
	}
	
	@Override
	public String getName () {
		return "Spooky dungeon";
	}
	
	@Override
	public boolean showName () {
		return true;
	}
	
	@Override
	public String getTexture () {
		return "hole";
	}
	
	@Override
	public ArrayList <Answer> getDialogue () {
		ArrayList <Answer> answers = new ArrayList <> ();
		
		answers.add (new Answer ("Enter the dungeon",
				"When you was standing in front of a door, you heard a silent scream behind the door. Do you really want to enter?",
				"dungeonbackground",
				new Answer ("Yes", Action.ENTER),
				new Answer ("No", Action.LEAVE)));
		
		answers.add (new Answer ("Leave", Action.LEAVE));
		
		return answers;
	}
	
	@Override
	public String getDescription () {
		return "Your team spotted a [#ff0000]" + getName () + "[#ffffff]. Seems like it's very deep, like your mother";
	}
}
