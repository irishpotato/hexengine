package com.noimg.hexengine.map.place.dungeon;

import com.noimg.hexengine.map.place.Place;

/**
 * Интерфейс, реализуемый подземельями
 */

public interface Dungeon extends Place {
	
	enum Action {
		LEAVE, ENTER
	}
	
	/*
	Level getLevel ();
	Level содержит информацию о данже
	*/
}
