package com.noimg.hexengine.map.place;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для работы с вариантами ответов в игровых местах и событиях.
 *
 * Как это работает:
 * Есть некий корневой объект Answer, в переменной answers которого содержатся еще 2 объекта Answer
 * (знаю, звучит как костыли), и выглядит это примерно так:
 *
 * new Answer ("Show me some puzzle",
 * 				"There are two chairs. On one there is a pie.
 * 				On the other, a dick. You have to sit on one,
 * 				and eat the other. Which do you choose?",
 *
 *		new Answer ("Sit on a dick and eat the pie", Action.TODDHOWARD),
 *		new Answer ("Sit on a pie and eat the dick", Action.SUPERMEATBOY)),
 *
 * Тоесть, если вопрос должен продолжать ветвление, то используется конструктор с параметрами:
 * Answer (String answerText, String questionText, Answer... answers)
 *
 * ... если вопрос должен продолжать ветвление и при этом должен менять фон, то
 * ... используется конструктор с добавленным параметром String background
 *
 * Если же вопрос должен совершить какое-либо действие, то используется конструктор:
 * Answer (String answerText, Enum<?> action)
 *
 * Причем "Enum<?> action" означает любой Enum в качестве типа. Т.к. ява не поддерживает наследование перечислений, мне пришлось сделать
 * таким образом (еще был вариант использовать int в качестве идентификатора действия, напр. 0 - всегда Leave, 1,2,3 и т.д. уже в
 * зависимости от вызывающего действие класса, но это вызвало бы путаницу, поэтому я сделал по другому).
 */

public class Answer {
	
	public final ArrayList <Answer> answers = new ArrayList <> ();
	public boolean hasAction;
	public boolean changeBackground;
	public String background;
	public String questionText;
	public final String answerText;
	public Enum<?> action;
	
	public Answer (String answerText, String questionText, Answer... answers) {
		this.answerText = answerText;
		this.questionText = questionText;
		this.answers.addAll (Arrays.asList (answers));
	}
	
	public Answer (String answerText, String questionText, String background,  Answer... answers) {
		this.answerText = answerText;
		this.questionText = questionText;
		this.background = background;
		changeBackground = true;
		this.answers.addAll (Arrays.asList (answers));
	}
	
	public Answer (String answerText, Enum<?> action) {
		this.answerText = answerText;
		this.action = action;
		hasAction = true;
	}
}
