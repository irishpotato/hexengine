package com.noimg.hexengine.map;

import com.badlogic.gdx.Gdx;
import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.map.tile.Forest;
import com.noimg.hexengine.map.tile.Plains;
import com.noimg.hexengine.map.tile.Savanna;
import com.noimg.hexengine.map.tile.Snow;
import com.noimg.hexengine.map.tile.Tile;
import com.noimg.hexengine.map.tile.Water;
import com.noimg.hexengine.utils.Point;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Map - класс с методами для работы непосредственно с тайл-картой
 */

public class Map {
	
	private final MapProperties properties;
	private final HashMap <Hex, Tile> map = new HashMap <> ();
	
	/**
	 * Переменная map является HashMap'ом: это означает, что для каждого значения всегда ставится в пару другое значение.
	 * Я посчитал наиболее простым присвоить каждой трехмерной (Hex) координате соответсвующий объект класса Tile, который,
	 * в свою очередь, хранит идентификатор тайла (и другие свойства)
	 * В дальнейшем, если необходимо получить объект Tile по мировым координатам, нужно просто найти эту координату в map.
	 */
	
	Map (short[][] ids, MapProperties properties) {
		this.properties = properties;
		
		for (int r = 0; r < properties.getWidth (); r++) {
			for (int q = 0; q < properties.getHeight (); q++) {
				Hex cube = Hex.offsetToCube (new Point (q, r));
				Tile tile;
				
				switch (ids[r][q]) {
					case 4:
						tile = new Snow ();
						break;
					
					case 6:
						tile = new Plains ();
						break;
					
					case 7:
						tile = new Water ();
						break;
					
					case 8:
						tile = new Savanna ();
						break;
					
					case 9:
						tile = new Forest ();
						break;
					
					default:
						tile = new Plains ();
						break;
				}
				
				map.put (cube, tile);
			}
		}
	}
	
	public ArrayList <Hex> getNeighbors (Hex hex) {
		ArrayList <Hex> neighbors = new ArrayList <> ();
		
		neighbors.add (Hex.add (hex, Hex.NORTH));
		neighbors.add (Hex.add (hex, Hex.NORTHEAST));
		neighbors.add (Hex.add (hex, Hex.SOUTHEAST));
		neighbors.add (Hex.add (hex, Hex.SOUTH));
		neighbors.add (Hex.add (hex, Hex.SOUTHWEST));
		neighbors.add (Hex.add (hex, Hex.NORTHWEST));
		
		return neighbors;
	}
	
	public ArrayList <Hex> getHexesInRange (Hex center, int range) {
		if (range == 1)
			Gdx.app.log ("Warning", "Better use getNeighbors instead of getHexesInRange with range 1");
		
		ArrayList <Hex> hexes = new ArrayList <> ();
		for (int dx = -range; dx <= range; dx++) {
			for (int dy = Math.max (-range, -dx - range); dy <= Math.min (range, -dx + range); dy++) {
				int dz = -dx - dy;
				hexes.add (new Hex (center.x + dx, center.y + dy, center.z + dz));
			}
		}
		
		return hexes;
	}
	
	public Tile getTile (Hex hex) {
		return map.get (hex);
	}
	
	public MapProperties getProperties () {
		return properties;
	}
	
}
