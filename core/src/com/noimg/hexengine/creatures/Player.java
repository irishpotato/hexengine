package com.noimg.hexengine.creatures;

import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.map.world.World;

/**
 * Класс для игрока
 */

public class Player extends Creature {
	
	public int foodAmount;
	
	public Player (World world, Hex hex) {
		super (world, hex);
		foodAmount = 100;
	}
	
	@Override
	public String getName () {
		return "The Player";
	}
	
	@Override
	public String getTexture () {
		return "player";
	}
}
