package com.noimg.hexengine.creatures;

import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.map.world.World;
import com.noimg.hexengine.utils.Point;

/**
 * Базовый класс для игровых существ
 */

public abstract class Creature {
	
	public Hex hex;
	public Point position;
	public boolean isMoving;
	private Hex moveHex;
	private float moveTime;
	private Point movePosition;
	private final World world;
	private boolean died;
	
	public abstract String getName ();
	
	public abstract String getTexture ();
	
	public Creature (World world, Hex hex) {
		this.hex = hex;
		this.world = world;
		
		position = Hex.hexToPixel (hex);
	}
	
	public void startMove (Hex destination) {
		if (isMoving || destination == hex)
			return;
		
		moveHex = destination;
		movePosition = Hex.hexToPixel (destination);
		isMoving = true;
	}
	
	/**
	 * move вызывается в методе render класса World (но, возможно, потом класс поменяется на контроллер существ)
	 * При этом move возвращает true, если существо закончило движение.
	 */
	public boolean move (float delta) {
		if (!isMoving)
			return false;
		
		moveTime += delta;
		
		float alpha = moveTime / (1 / HexEngine.CREATURE_MOVE_SPEED);
		position = Point.lerp (position, movePosition, alpha);
		
		if (alpha > (1 / HexEngine.CREATURE_MOVE_SPEED)) {
			position = movePosition;
			moveTime = 0;
			hex = moveHex;
			position = Hex.hexToPixel (hex);
			isMoving = false;
			return true;
		}
		
		return false;
	}
	
	public void die () {
		died = true;
	}
	
	public boolean isDied () {
		return died;
	}
	
	/**
	 * Может ли существо дойти до переданного гексагона
	 */
	public boolean canMoveTo (Hex hex) {
		return (world.getMap ().getNeighbors (this.hex).contains (hex) &&
				world.getMap ().getTile (hex).foodNeed () != 0) || HexEngine.JESUS_MODE;
	}
}
