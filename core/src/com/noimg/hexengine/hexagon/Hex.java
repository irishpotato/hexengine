package com.noimg.hexengine.hexagon;

import com.noimg.hexengine.utils.Point;

/**
 * Hex - класс для хранения трехмерных позиций гексагонов и их обработки
 * <p>
 * Экскурс в координатную систему:
 * Есть трехмерные (x-y-z, cube) координаты. Тут, не смотря на двумерность игры,
 * они используются как основные, т.к. с ними намного проще работать в гексагональной карте. При этом x+y+z = 0 всегда
 *
 * Есть двумерные (x-y, offset) координаты. В основном это координаты курсора на экране и другие
 * двумерные вещи: с ними работает довольно простой класс Point.
 * <p>
 * Более подробно тут -- http://www.redblobgames.com/grids/hexagons/
 */

public class Hex {
	
	public final static Hex CENTER = new Hex (0, 0, 0);
	public final static Hex NORTH = new Hex (0, 1, -1);
	public final static Hex NORTHEAST = new Hex (1, 0, -1);
	public final static Hex SOUTHEAST = new Hex (1, -1, 0);
	public final static Hex SOUTH = new Hex (0, -1, 1);
	public final static Hex SOUTHWEST = new Hex (-1, 1, 0);
	public final static Hex NORTHWEST = new Hex (-1, 0, 1);
	public final short x;
	public final short y;
	public final short z;
	private final short hash;
	
	public Hex (int x, int y, int z) {
		this.x = (short) x;
		this.y = (short) y;
		this.z = (short) z;
		
		hash = (short) (x ^ y);
	}
	
	public Hex (int x, int y) {
		this.x = (short) x;
		this.y = (short) y;
		this.z = (short)(-x-y);
		
		hash = (short) (x ^ y);
	}
	
	public static Point cubeToOffset (Hex hex) {
		int x = (hex.x);
		int y = -(hex.z + hex.x / 2);
		
		return new Point (x, y);
	}
	
	public static Hex offsetToCube (Point point) {
		double x = point.y;
		double y = (point.x - point.y / 2) + (x % 2 == 0 ? 1 : 0);
		double z = -y - x;
		
		return new Hex ((int) x, (int) y, (int) z);
	}
	
	public static Hex pixelToHex (Point point) {
		double x = (point.x - 4) / 32;
		double y = ((point.y - (x % 2 == 0 ? 17 : 0)) / 34) - 1;
		
		return offsetToCube (new Point ((int) y, (int) x));
	}
	
	public static Point hexToPixel (Hex hex) {
		int x = hex.x * 32;
		int y = (hex.y * 34) + x / 2 + hex.x;
		
		return new Point (x, y);
	}
	
	public static float distance (Hex a, Hex b) {
		return (Math.abs (a.x - b.x) + Math.abs (a.y - b.y) + Math.abs (a.z - b.z)) / 2f;
	}
	
	public static Hex add (Hex a, Hex b) {
		return new Hex (a.x + b.x, a.y + b.y, a.z + b.z);
	}
	
	@Override
	public final String toString () {
		return "(" + x + ", " + y + ", " + z + ")";
	}
	
	/**
	 * Переопределение hashCode и equals необходимо для нормальной работы HashMap'а в классе Map, а так же для
	 * удобного сравнивания гексагональных позиций
	 */
	@Override
	public int hashCode () {
		return hash;
	}
	
	@Override
	public boolean equals (Object o) {
		if (!(o instanceof Hex)) {
			return false;
		}
		
		Hex hex = (Hex) o;
		return x == hex.x && y == hex.y && z == hex.z;
	}
}
