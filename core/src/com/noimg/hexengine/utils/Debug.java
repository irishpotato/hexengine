package com.noimg.hexengine.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.noimg.hexengine.Assets;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.hexagon.Hex;
import com.noimg.hexengine.screens.GameScreen;

/**
 * Debug - название говорит само за себя
 * Выводит на экран текстовую информацию, необходимую для разрабокти.
 * Так же имеет методы для работы с тайл-картой, такие как отрисовка на гексагонах их координат или отрисовка
 * координатных осей
 */
public class Debug {
	
	public final SpriteBatch batch;
	private final BitmapFont font;
	
	public Debug () {
		batch = new SpriteBatch ();
		font = HexEngine.assets.get (Assets.FONT_NEWPIXEL, BitmapFont.class);
	}
	
	public void debug (GameScreen game) {
		batch.begin ();
		
		Vector3 mousePosition = new Vector3 (Gdx.input.getX (), Gdx.input.getY (), 0);
		game.camera.unproject (mousePosition);
		String debug = "Jesus mode: " + (HexEngine.JESUS_MODE ? "ON" : "OFF") + "\nFood amount: " + game.getWorld ().getPlayer ().foodAmount + "\nWorld time: " + game.time + "\nFps: " + Gdx.graphics.getFramesPerSecond ();
		
		font.getData ().setScale (2, 2);
		font.setColor (Color.BLACK);        // Тень
		font.draw (batch, debug, 15, 73);    // Тень
		font.setColor (Color.WHITE);
		font.draw (batch, debug, 15, 75);
		batch.end ();
	}
	
	public void drawCoordinates (SpriteBatch batch, Hex hex, int drawx, int drawy) {
		font.getData ().setScale (1, 1);
		font.setColor (Color.BLACK);
		font.draw (batch, hex.x + "\n" + hex.y + "\n" + hex.z, drawx + 8, drawy + 25);
	}
	
	public void drawAxis (SpriteBatch batch, Hex hex) {
		if (hex.x == 0)
			batch.setColor (new Color (0.94f, 0.23f, 0.22f, 1f));
		if (hex.y == 0)
			batch.setColor (new Color (0.23f, 0.94f, 0.22f, 1f));
		if (hex.z == 0)
			batch.setColor (new Color (0.22f, 0.23f, 0.94f, 1f));
	}
	
}
