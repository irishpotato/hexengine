package com.noimg.hexengine.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * GzipUtil - утилита для работы со сжатыми строками, необходимая MapLoader'у для расшифровки .tmx карт
 */

public class GzipUtil {
	
	public static byte[] zip (final String str) {
		if ((str == null) || (str.length () == 0)) {
			throw new IllegalArgumentException ("Cannot zip null or empty string");
		}
		
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ();
			GZIPOutputStream gzipOutputStream = new GZIPOutputStream (byteArrayOutputStream);
			gzipOutputStream.write (str.getBytes ("UTF-8"));
			
			return byteArrayOutputStream.toByteArray ();
		} catch (IOException e) {
			throw new RuntimeException ("Failed to zip content", e);
		}
	}
	
	public static String unzip (final byte[] compressed) {
		if ((compressed == null) || (compressed.length == 0)) {
			throw new IllegalArgumentException ("Cannot unzip null or empty bytes");
		}
		if (!isZipped (compressed)) {
			return new String (compressed);
		}
		
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream (compressed);
			GZIPInputStream gzipInputStream = new GZIPInputStream (byteArrayInputStream);
			InputStreamReader inputStreamReader = new InputStreamReader (gzipInputStream, "UTF-8");
			BufferedReader bufferedReader = new BufferedReader (inputStreamReader);
								
			StringBuilder output = new StringBuilder ();
			String line;
			while ((line = bufferedReader.readLine ()) != null) {
				output.append (line);
			}
			
			return output.toString ();
		} catch (IOException e) {
			throw new RuntimeException ("Failed to unzip content", e);
		}
	}
	
	public static boolean isZipped (final byte[] compressed) {
		return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
	}
}