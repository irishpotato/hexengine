package com.noimg.hexengine.utils;

import com.badlogic.gdx.math.MathUtils;

/**
 * В силу того что дефолтный Vector2 у libgdx мне не нравится своей громоздкостью, я решил написать для этого свой класс
 * в нем нету ничего более чем x, y и переопределение toString для удобности
 */

public class Point {
	public final int x;
	public final int y;
	
	public Point (int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public static Point lerp (Point start, Point end, float time) {
		return new Point((int) MathUtils.lerp (start.x, end.x, time), (int) MathUtils.lerp (start.y, end.y, time));
	}
	
	public static float distance (Point a, Point b) {
		return (float) Math.sqrt ((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
	}
	
	@Override
	public String toString () {
		return "(" + x + ", " + y + ")";
	}
}
