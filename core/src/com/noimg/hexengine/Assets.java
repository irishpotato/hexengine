package com.noimg.hexengine;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Класс с игровыми ресурсами. Использует AssetManager для возможности отрисовки загрузочного экрана во время
 * загрузки ассетов.
 */
public class Assets {
	
	// Папки
	private static final String FLDR_TEXTURE = "textures/";
	// Отдельные текстуры
	public static final String TEX_TITLE = FLDR_TEXTURE + "title.png";
	// Атласы текстур
	public static final String ATL_OBJECT = FLDR_TEXTURE + "objectmap.pack";
	public static final String ATL_TILE = FLDR_TEXTURE + "tilemap.pack";
	public static final String ATL_UI = FLDR_TEXTURE + "uimap.pack";
	private static final String FLDR_FONT = "fonts/";
	// Шрифты
	public static final String FONT_PIXEL = FLDR_FONT + "pixel.fnt";
	public static final String FONT_NEWPIXEL = FLDR_FONT + "newpixel.fnt";
	public static final String FONT_SILOMBOL = FLDR_FONT + "SilomBol.fnt";
	public static final String FONT_RUNESCAPE = FLDR_FONT + "runescape_uf.fnt";
	public static final String FONT_CODERSPLUX = FLDR_FONT + "coders_crux.fnt";
	
	// Разное
	public static final String UISKIN = FLDR_TEXTURE + "uiskin.json";
	
	// Менеджер ассетов
	public AssetManager manager;
	
	public void loadImportant () {
		manager = new AssetManager (new InternalFileHandleResolver ());
		manager.load (TEX_TITLE, Texture.class);
	}
	
	public void load () {
		manager.load (UISKIN, Skin.class, new SkinLoader.SkinParameter(FLDR_TEXTURE + "/uimap.pack"));
		manager.load (ATL_TILE, TextureAtlas.class);
		manager.load (ATL_OBJECT, TextureAtlas.class);
		manager.load (ATL_UI, TextureAtlas.class);
		
		manager.load (FONT_PIXEL, BitmapFont.class);
		manager.load (FONT_NEWPIXEL, BitmapFont.class);
		manager.load (FONT_SILOMBOL, BitmapFont.class);
		manager.load (FONT_RUNESCAPE, BitmapFont.class);
		manager.load (FONT_CODERSPLUX, BitmapFont.class);
	}
	
	public <T> T get (String path, Class <T> type) {
		return manager.get (path, type);
	}
}
