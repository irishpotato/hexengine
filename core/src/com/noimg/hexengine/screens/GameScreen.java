package com.noimg.hexengine.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.map.world.Overworld;
import com.noimg.hexengine.map.world.World;

/**
 * GameScreen является игровым экраном, в котором обрабатывается основная игровая логика
 */

public class GameScreen implements Screen {
	
	public final OrthographicCamera camera;
	public boolean zoomTransition;
	private final World world;
	private float camAnimTime;
	public int time;
	private float zoomBeforeTransition;
	
	public GameScreen () {
		camera = new OrthographicCamera (200, 238) {
			@Override
			public void update () {
				super.update ();
				zoom = Math.max (HexEngine.ZOOM_MIN, Math.min (HexEngine.ZOOM_MAX, zoom));
				position.x = (int)position.x;
				position.y = (int)position.y;
			}
		};
		
		camera.zoom = (HexEngine.ZOOM_MAX + HexEngine.ZOOM_MIN) / 3;
		world = new Overworld (this);
	}
	
	@Override
	public void show () {

	}
	
	@Override
	public void render (float delta) {
		world.render (delta);
		
		if (zoomTransition) {
			camAnimTime += delta;
			
			camera.zoom = MathUtils.lerp (0.1f, 0.5f, camAnimTime / 0.6f);
			
			if (camera.zoom >= zoomBeforeTransition) {
				camera.zoom = zoomBeforeTransition;
				zoomTransition = false;
				camAnimTime = 0;
			}
			
			camera.update ();
		} else {
			zoomBeforeTransition = camera.zoom;
		}
	}
	
	@Override
	public void resize (int width, int height) {
		world.resize (width, height);
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.update ();
	}
	
	@Override
	public void pause () {
		
	}
	
	@Override
	public void resume () {
		
	}
	
	@Override
	public void hide () {
		
	}
	
	@Override
	public void dispose () {
		world.dispose ();
	}
	
	public World getWorld () {
		return world;
	}
}
