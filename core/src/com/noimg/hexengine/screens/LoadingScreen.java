package com.noimg.hexengine.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.noimg.hexengine.Assets;
import com.noimg.hexengine.HexEngine;

/**
 * LoadingScreen - загрузочный экран
 */

public class LoadingScreen implements Screen {
	
	private final ShapeRenderer shapeRenderer = new ShapeRenderer ();
	private SpriteBatch batch;
	private Texture title;
	
	@Override
	public void show () {
		HexEngine.assets.loadImportant ();
		HexEngine.assets.manager.finishLoading ();
		title = HexEngine.assets.get (Assets.TEX_TITLE, Texture.class);
		HexEngine.assets.load ();
		batch = new SpriteBatch ();
	}
	
	@Override
	public void render (float delta) {
		float progress = HexEngine.assets.manager.getProgress ();
		shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);
		shapeRenderer.setColor (Color.WHITE);
		shapeRenderer.rect (10, 20, 650 * progress, 15);
		shapeRenderer.end ();
		
		batch.begin ();
		batch.draw (title, 20, 50);
		batch.end ();
		
		if (HexEngine.assets.manager.update ()) {
			HexEngine listener = (HexEngine) Gdx.app.getApplicationListener ();
			GameScreen game = new GameScreen ();
			HexEngine.game = game;
			listener.setScreen (game);
		}
	}
	
	@Override
	public void resize (int width, int height) {
		
	}
	
	@Override
	public void pause () {
		
	}
	
	@Override
	public void resume () {
		
	}
	
	@Override
	public void hide () {
		
	}
	
	@Override
	public void dispose () {
		batch.dispose ();
	}
}
