package com.noimg.hexengine.screens.event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimg.hexengine.Assets;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.map.place.dungeon.Dungeon;
import com.noimg.hexengine.map.place.event.Caravan;
import com.noimg.hexengine.map.place.event.WorldEvent;

/**
 * Базовый класс для интерфейсов игровых локаций
 * Всего типов интерфейсов есть несколько:
 * EventScreen для "вопрос-ответ"
 * DialogueScreen для диалогов с NPC
 * SettlementScreen для интерфейса поселения
 *
 * В диалогах с NPC лицо будет наклеено поверх фона
 */

public abstract class EventScreenBase implements Screen {
	
	protected final WorldEvent event;
	protected final Stage stage;
	protected final Skin skin;
	
	public EventScreenBase (final WorldEvent event) {
		this.event = event;
		
		Viewport viewport;
		OrthographicCamera camera;
		
		skin = HexEngine.assets.get (Assets.UISKIN, Skin.class);
		skin.getFont("default-font").getData().markupEnabled = true;
		
		camera = new OrthographicCamera(212, 284);
		viewport = new ExtendViewport (212, 284, camera);
		viewport.apply ();
		camera.update ();
		
		stage = new Stage (viewport);
		Gdx.input.setInputProcessor (stage);
	}
	
	final void handleAction (Enum<?> action) {
		if (action.ordinal () == 0) HexEngine.leavePlace ();
		
		if (event instanceof Dungeon) {
			switch ((Dungeon.Action) action) {
				case ENTER:
					System.out.println ("enter");
					break;
			}
		}
		
		else if (event instanceof Caravan) {
			switch ((Caravan.Action) action) {
				case TALK:
					System.out.println ("talk");
					break;
				case TRADE:
					System.out.println ("trade");
					break;
				case ATTACK:
					System.out.println ("attack");
					break;
			}
		}
		
		else Gdx.app.error ("Error", "Can't find action " + action);
	}
	
	@Override
	public void show () {
		
	}
	
	@Override
	public void render (float delta) {
		Gdx.gl.glClear (GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor (4f / 255f, 4f / 255f, 9f / 255f, 1);
		stage.act (Gdx.graphics.getDeltaTime ());
		stage.draw ();
	}
	
	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(Math.round(width / 2f) * 2, Math.round(height / 2f) * 2);
		stage.getCamera().position.set(212/2, 284/2, 0);
		stage.getCamera().update();
	}
	
	
	@Override
	public void pause () {
		
	}
	
	@Override
	public void resume () {
		
	}
	
	@Override
	public void hide () {
		
	}
	
	@Override
	public void dispose () {
		stage.dispose ();
	}
}
