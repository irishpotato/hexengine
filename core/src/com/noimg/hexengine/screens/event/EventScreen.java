package com.noimg.hexengine.screens.event;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pool;
import com.noimg.hexengine.Assets;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.map.place.Answer;
import com.noimg.hexengine.map.place.event.WorldEvent;

import java.util.ArrayList;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.noimg.hexengine.HexEngine.UI_ANIMATION_SPEED;

public class EventScreen extends EventScreenBase {
	
	private ArrayList <Answer> answers = new ArrayList <> ();
	private Table backgroundTable;
	private Table mainTable;
	private Label description;
	private Label title;
	private Image background;
	public static final float STARTANIMPEED = 0.8f;
	
	public EventScreen (final WorldEvent event) {
		super (event);
		
		answers.addAll (event.getDialogue ());
	}
	
	@Override
	public void show () {
		// Инициализируем вид
		initLook ();
		// Инициализируем анимации
		initAnimations ();
		
		stage.addActor (mainTable);
		stage.addActor (backgroundTable);
	}
	
	private void initAnimations () {
		//todo понять как это работает
		Pool<MoveToAction> pool = new Pool<MoveToAction> () {
			protected MoveToAction newObject () {
				MoveToAction move = new MoveToAction ();
				move.setPosition(0, 142 - 35);
				move.setDuration(STARTANIMPEED * (1/UI_ANIMATION_SPEED));
				move.setInterpolation(Interpolation.smooth2);
				return move;
			}
		};
		
		MoveToAction moveUp = pool.obtain();
		title.addAction(sequence(Actions.delay (STARTANIMPEED), Actions.fadeIn (STARTANIMPEED * (1/UI_ANIMATION_SPEED)), moveUp));
		moveUp = pool.obtain();
		background.addAction(sequence(Actions.fadeIn (STARTANIMPEED * (1/UI_ANIMATION_SPEED)), Actions.delay (STARTANIMPEED), moveUp));
	}
	
	private void initLook () {
		background = new Image (HexEngine.assets.get (Assets.ATL_UI, TextureAtlas.class).findRegion ("background"));
		background.setColor (new Color(1, 1, 1, 0));
		
		title = new Label (event.getName (), skin, "title");
		title.setColor (new Color(1, 1, 1, 0));
		initLabel (title);
		description = new Label (event.getDescription (), skin, "default");
		initLabel (description);

		// Инициализируем столы и расставляем по ним актеров
		mainTable = new Table (skin);
		mainTable.top ();
		//mainTable.debug ();
		updateMainTable ();
		mainTable.setColor (Color.CLEAR);
		mainTable.addAction (sequence(Actions.delay (STARTANIMPEED * 2 + 0.3f), Actions.fadeIn (STARTANIMPEED * (1/UI_ANIMATION_SPEED))));
		mainTable.setBounds (0, 0, stage.getViewport ().getWorldWidth (), stage.getViewport ().getWorldHeight () / 2 + 35);
		
		backgroundTable = new Table (skin);
		//backgroundTable.debug ();
		backgroundTable.center ();
		backgroundTable.stack (background, title).size (212, 71).row ();
		//backgroundTable.setBounds (0, 0, stage.getViewport ().getScreenWidth () / 2, stage.getViewport ().getScreenHeight () / 2);
		backgroundTable.setFillParent (true);
	}
	
	@Override
	public void render (float delta) {
		super.render (delta);
	}
	
	@Override
	public void resize (int width, int height) {
		super.resize (width, height);
		//int viewportWidth = stage.getViewport ().getScreenWidth () / 2;
		//int viewportHeight = stage.getViewport ().getScreenHeight () / 2;
		//mainTable.setWidth (viewportWidth);
		//description.setWidth (viewportWidth - 5);
		//title.setWidth (viewportWidth - 5);
		//mainTable.setBounds (0, 0, viewportWidth, viewportHeight / 2 + 35);
		
	}
	
	private void initLabel (Label label) {
		label.setWrap (true);
		label.setAlignment (Align.center);
		label.setWidth (stage.getViewport ().getWorldWidth () - 5);
		label.setFontScale (1);
		label.setTouchable (Touchable.disabled);
	}
	
	private void selectAnswer (Answer answer) {
		if (!answers.contains (answer) || answer.answers.size () == 0)
			return;
		
		if (answer.changeBackground)
			background.setDrawable (new TextureRegionDrawable (
							HexEngine.assets.get (Assets.ATL_UI, TextureAtlas.class).findRegion (answer.background)));
		
		answers = answer.answers;
		description.setText (answer.questionText);
		updateMainTable ();
	}
	
	private void updateMainTable () {
		ScrollPane scrollPane = new ScrollPane (description);
		scrollPane.setPosition (0, 100);
		
		mainTable.clear ();
		mainTable.add (scrollPane).width (stage.getViewport ().getWorldWidth () - 5).padBottom (10).maxHeight (80).minHeight (40).row ();
		
		for (final Answer answer : answers) {
			TextButton button = new TextButton ("... " + answer.answerText, skin);
			button.getLabel ().setWrap (true);
			button.getLabel ().setWidth (stage.getViewport ().getWorldWidth () - 5);
			button.getLabel ().setAlignment (Align.left);
			button.getLabel ().setFontScale (1);
			
			button.addListener (new ClickListener () {
				public void clicked (InputEvent e, float x, float y) {
					if (!answer.hasAction) {
						selectAnswer (answer);
						return;
					}
					
					handleAction (answer.action);
				}
			});
			
			mainTable.add (button).width (stage.getViewport ().getWorldWidth () - 5).minHeight (22).row ();
		}
	}
}
