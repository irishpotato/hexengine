package com.noimg.hexengine.screens.event.settlement;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.noimg.hexengine.Assets;
import com.noimg.hexengine.HexEngine;
import com.noimg.hexengine.map.place.Place;
import com.noimg.hexengine.map.place.settlement.Settlement;
import com.noimg.hexengine.screens.event.EventScreenBase;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class SettlementScreen extends EventScreenBase {
	
	private Table titleTable;
	private Label title;
	private Table backgroundTable;
	private Table buttonsTable;
	private final ImageButton[] sideButtons;
	private final Settlement settlement;
	
	public SettlementScreen (Place place) {
		super (place);
		settlement = (Settlement)place;
		
		// Инициализируем кнопки
		sideButtons = new ImageButton[settlement.getPlaces ().length];
		for (int i = 0; i < settlement.getPlaces ().length; i++) {
			sideButtons[i] = initSideButton (settlement.getPlaces ()[i]);
		}
	}
	
	//todo сделать единый класс Action или использовать уже существующий
	private void goToPlace (Settlement.Place place) {
		switch (place) {
			case TAVERN:
				System.out.println ("You entered Tavern!");
				break;
			
			case SHOP:
				System.out.println ("You entered Shop!");
				break;
		}
	}
	
	//fixme мб как то объединить инициализации кнопок и handleAction'ы во всех классах, наследуемых от EventScreenBase?
	private ImageButton initSideButton (final Settlement.Place cityPlace) {
		Drawable drawable = new TextureRegionDrawable (
				new TextureRegion (HexEngine.assets.get (Assets.ATL_UI, TextureAtlas.class)
						.findRegion (cityPlace.toString ().toLowerCase () + "button")));
		
		ImageButton sideButton = new ImageButton(drawable);
		
		MoveToAction action = Actions.action(MoveToAction.class);
		action.setPosition(143, 0);
		action.setDuration(1);
		action.setInterpolation(Interpolation.bounceOut);
		
		sideButton.addAction(sequence(action, run(new Runnable() {
			public void run () {
				
			}
		})));
		
		sideButton.addListener (new ClickListener () {
			public void clicked (InputEvent e, float x, float y) {
				goToPlace (cityPlace);
			}
		});
		
		return sideButton;
	}
	
	@Override
	public void show () {
		// Инициализируем надписи
		title = new Label (event.getName (), skin);
		title.setWrap (true);
		title.setAlignment (Align.topRight);
		title.setWidth (120);
		title.setTouchable (Touchable.disabled);
		
		titleTable = new Table ();
		titleTable.add (title).width (120);
		titleTable.setPosition (stage.getWidth () / 2 + 10, stage.getHeight () / 2 + 90);
		
		backgroundTable = new Table ();
		backgroundTable.add (new Image (HexEngine.assets.get (Assets.ATL_UI, TextureAtlas.class).findRegion (settlement.getEntryBackground ()))).size (166, 284);
		backgroundTable.setPosition (stage.getWidth () / 2 + 23, stage.getHeight () / 2);
		
		buttonsTable = new Table ();
		
		// Добавляем боковые кнопки
		for (int i = 0; i < settlement.getPlaces ().length; i++) {
			buttonsTable.add (sideButtons[i]).row ();
			//buttonsTable.add (new Image ()).size (46, 56).row ();
			buttonsTable.setPosition (23, 28 * (i + 1));
		}
		
		stage.addActor (backgroundTable);
		stage.addActor (titleTable);
		stage.addActor (buttonsTable);
	}
}
