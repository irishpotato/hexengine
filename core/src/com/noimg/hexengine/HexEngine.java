package com.noimg.hexengine;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.noimg.hexengine.map.place.Place;
import com.noimg.hexengine.map.place.dungeon.Dungeon;
import com.noimg.hexengine.map.place.event.WorldEvent;
import com.noimg.hexengine.map.place.settlement.Settlement;
import com.noimg.hexengine.screens.GameScreen;
import com.noimg.hexengine.screens.LoadingScreen;
import com.noimg.hexengine.screens.event.EventScreen;
import com.noimg.hexengine.screens.event.EventScreenBase;
import com.noimg.hexengine.screens.event.settlement.SettlementScreen;

/**
 * HexEngine является "входной точкой" игры и может работать с ассетами или с игровыми экранами
 * Игровым экраном может являтся меню игры, сама игра или, например, окно города/деревни/etc
 */

public class HexEngine extends Game {
	
	public static Assets assets;
	public static GameScreen game;
	
	/** Статичные игровые параметры **/
	public static final float ZOOM_MIN = 0.2f;				//default: 0.2f
	public static final int RENDER_RANGE = 6;				//default: 6
	public static final float CREATURE_MOVE_SPEED = 1.7f;	//default: 1.7f
	public static final float UI_ANIMATION_SPEED = 1f;		//default: 1f
	public static final float CAM_ZOOM_SPEED = 0.04f;		//default: 0.04f
	public static final float ZOOM_MAX = 0.8f;				//default: 0.8f
	public static final float DAYSPEED = 3f;				//default: 3f
	public static boolean JESUS_MODE = false;				//default: false
	public static boolean SHOW_DEBUG = true;				//default: false
	public static boolean SHOW_AXIS = false;				//default: false
	public static boolean SHOW_HEX_COORD = false;			//default: false
	
	@Override
	public void create () {
		assets = new Assets ();
		setScreen (new LoadingScreen ());
	}
	
	@Override
	public void render () {
		super.render ();
	}
	
	public static void startEvent (WorldEvent event) {
		HexEngine listener = (HexEngine) Gdx.app.getApplicationListener ();
		listener.setScreen (new EventScreen (event));
	}
	
	public static void enterPlace (Place place) {
		EventScreenBase screen = null;
		
		if (place instanceof Dungeon) screen = new EventScreen (place);
		else if (place instanceof Settlement) screen = new SettlementScreen (place);
		
		HexEngine listener = (HexEngine) Gdx.app.getApplicationListener ();
		listener.setScreen (screen);
	}
	
	public static void leavePlace () {
		HexEngine listener = (HexEngine) Gdx.app.getApplicationListener ();
		if (!(listener.getScreen () instanceof EventScreenBase))
			return;
		
		listener.setScreen (HexEngine.game);
		Gdx.input.setInputProcessor (HexEngine.game.getWorld ().getInput ());
		HexEngine.game.zoomTransition = true;
	}
	
	@Override
	public void dispose () {
		super.dispose ();
		assets.manager.dispose ();
	}
}
